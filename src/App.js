
import React from 'react'
import Bigcalendar from  '../src/components/calendar/bigCalendar/Bigcalendar'
import  '../src/app.css'

function App() {
  return (
    <div className="App">
    <header>
      <div id="logo">
        <span className="icon">date_range</span>
        <span>
          react<b>calendar</b>
        </span>
      </div>
    </header>
    <main>
      <Bigcalendar />
    </main>
  </div>
  );
}

export default App;
